package services

import (
	"context"
	"errors"

	"Todo/internal/storage"
	"Todo/models"
)

type TodoStorage interface {
	GetTodos(ctx context.Context) ([]models.Todo, error)
	CreateTodo(ctx context.Context, todo *models.Todo) (int64, error)
}

type Todo struct {
	st TodoStorage
}

func NewTodoService(st TodoStorage) *Todo {
	return &Todo{st: st}
}

func (t *Todo) GetAll(ctx context.Context) ([]models.Todo, error) {
	todos, err := t.st.GetTodos(ctx)
	if errors.Is(err, storage.ErrNoElements) {
		return []models.Todo{}, nil
	}
	if err != nil {
		return nil, err
	}

	return todos, nil
}

func (t *Todo) Create(ctx context.Context, todo *models.Todo) error {
	_, err := t.st.CreateTodo(ctx, todo)
	return err
}
