package sqlite

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	_ "github.com/mattn/go-sqlite3"

	"Todo/internal/storage"
	"Todo/models"
)

type Storage struct {
	db *sql.DB
}

func NewStorage(filePath string) (*Storage, error) {
	db, openErr := sql.Open("sqlite3", filePath)
	if openErr != nil {
		return nil, openErr
	}

	return &Storage{
		db: db,
	}, nil
}

func (s *Storage) ExecMigrations(query ...string) error {
	tx, err := s.db.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()

	for _, v := range query {
		_, err = tx.Exec(v)
		if err != nil {
			_ = tx.Rollback()
		}
	}

	return tx.Commit()
}

func (s *Storage) GetTodos(ctx context.Context) ([]models.Todo, error) {
	stmt, err := s.db.PrepareContext(ctx, `
		SELECT id, task, comment, is_done, created_at, deleted_at
		FROM todos
		WHERE deleted_at IS NULL
	`)
	if err != nil {
		return nil, fmt.Errorf("failed prepare query: %w", err)
	}

	rows, err := stmt.QueryContext(ctx)
	if err == sql.ErrNoRows {
		return nil, fmt.Errorf("table todos is empty: %w", storage.ErrNoElements)
	}
	if err != nil {
		return nil, fmt.Errorf("failed select: %w", err)
	}

	todos := make([]models.Todo, 0)
	for rows.Next() {
		todo := models.Todo{}

		err = rows.Scan(&todo.ID, &todo.Task, &todo.Comment, &todo.IsDone, &todo.CreateAt, &todo.DeletedAt)
		if err != nil {
			return nil, fmt.Errorf("failed scan: %w", err)
		}

		todos = append(todos, todo)
	}

	return todos, nil
}

func (s *Storage) CreateTodo(ctx context.Context, todo *models.Todo) (int64, error) {
	stmt, err := s.db.PrepareContext(ctx, `
		INSERT INTO todos (task, comment, is_done, created_at) 
		VALUES (?, ?, ?, ?)
		RETURNING id
	`)
	if err != nil {
		return 0, fmt.Errorf("failed prepare query: %w", err)
	}

	row := stmt.QueryRowContext(ctx, todo.Task, todo.Comment, false, time.Now())
	err = row.Err()
	if err != nil {
		return 0, fmt.Errorf("failed exec: %w", err)
	}

	var res int64

	err = row.Scan(&res)
	if err != nil {
		return 0, fmt.Errorf("failed scan: %w", err)
	}

	return res, nil
}

func (s *Storage) Close() error {
	return s.db.Close()
}
