package app

import (
	"context"
	_ "embed"
	"fmt"
	"net/http"

	"Todo/internal/config"
	"Todo/internal/http_server/handlers"
	"Todo/internal/http_server/middleware"
	"Todo/internal/services"
	"Todo/internal/storage/sqlite"
)

// embed ./migrations/init.sql - нельзя
//
//go:embed migrations/init.sql
var initScript string

type App struct {
	cfg    *config.Config
	store  *sqlite.Storage
	server *http.Server

	todoService *services.Todo
}

func NewApp(cfg *config.Config) *App {
	store, err := initStore(cfg)
	if err != nil {
		fmt.Println(fmt.Errorf("fatal: init db %w", err))
		panic("failed init db")
	}

	err = migrate(store)
	if err != nil {
		fmt.Println(fmt.Errorf("fatal: migrate %w", err))
		panic("failed migrate")
	}

	todoService := services.NewTodoService(store)

	address := fmt.Sprintf(":%d", cfg.Http.Port)

	a := &App{
		cfg:         cfg,
		store:       store,
		todoService: todoService,
		server: &http.Server{
			Addr:    address,
			Handler: initApi(todoService),
		},
	}

	return a
}

func (a *App) Run() {
	go func() {
		err := a.server.ListenAndServe()
		if err != nil {
			fmt.Println(err)
		}
	}()
}

func initStore(cfg *config.Config) (*sqlite.Storage, error) {
	db, err := sqlite.NewStorage(cfg.DB.FilePath)
	if err != nil {
		return nil, err
	}

	return db, nil
}

func migrate(st *sqlite.Storage) error {
	return st.ExecMigrations(initScript)
}

func initApi(s *services.Todo) http.Handler {
	mux := http.NewServeMux()
	todoHandlers := handlers.NewTodoHandlers(s)

	mux.HandleFunc("/todo/get", todoHandlers.GetHandler)
	mux.HandleFunc("/todo/create", todoHandlers.CreateHandler)

	handler := middleware.Logging(mux)

	return handler
}

func (a *App) Stop(ctx context.Context) {
	fmt.Println(a.store.ExecMigrations())

	fmt.Println(a.server.Shutdown(ctx))
}
