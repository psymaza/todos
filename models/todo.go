package models

type Todo struct {
	Base
	Task    string `json:"task"`
	Comment string `json:"comment"`
	IsDone  bool   `json:"isDone"`
}
