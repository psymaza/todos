package models

import "time"

type Base struct {
	ID        int64      `json:"id"`
	CreateAt  time.Time  `json:"createAt"`
	DeletedAt *time.Time `json:"deletedAt"`
}
